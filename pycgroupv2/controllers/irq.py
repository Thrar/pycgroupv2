from __future__ import annotations

import typing

if typing.TYPE_CHECKING:
    from .formatters import Formatter

from .abstract_controller import AbstractController
from . import formatters as fmt


class IrqController(AbstractController):
    PREFIX = 'irq'

    FILES: dict[str, Formatter] = dict(
        pressure=fmt.nested_keys,
    )
