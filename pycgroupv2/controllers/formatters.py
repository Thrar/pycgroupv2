from __future__ import annotations

import typing

Formatter = typing.Callable[[str], typing.Any]


def single_integer(int_str: str) -> int:
    return int(int_str)


def string_list(str_list: str) -> typing.Iterable[str]:
    return str_list.split()


def integer_list(int_list: str) -> typing.Iterable[int]:
    return [int(i) for i in int_list.split()]


def nested_keys(nested_keys_str: str) -> dict[str, dict[str, int | float]]:
    keys = {}

    for line in nested_keys_str.split('\n'):
        if not line:
            continue

        line_key, line_data = tuple(line.split(maxsplit=1))

        keys[line_key] = {}

        for key, value_str in [kv.split('=') for kv in line_data.split()]:
            try:
                value = int(value_str)
            except ValueError:
                value = float(value_str)

            keys[line_key][key] = value

    return keys
