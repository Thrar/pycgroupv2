from __future__ import annotations

import typing

if typing.TYPE_CHECKING:
    from .formatters import Formatter

from .abstract_controller import AbstractController
from . import formatters as fmt


class MemoryController(AbstractController):
    PREFIX = 'memory'

    FILES: dict[str, Formatter] = dict(
        current=fmt.single_integer,
    )
