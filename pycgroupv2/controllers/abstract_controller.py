from __future__ import annotations

import os
import typing

if typing.TYPE_CHECKING:
    from ..cgroup import CGroup
    from .formatters import Formatter

from abc import ABCMeta

from ..error import ControllerFileNotFoundError, ControllerReadError


class AbstractController(metaclass=ABCMeta):
    PREFIX: str = 'abstract'
    FILES: dict[str, Formatter]

    def __init__(self, cgroup: CGroup):
        self.cgroup = cgroup

    def __getattr__(self, item):
        try:
            with open(os.path.join(self.cgroup.path, self.make_filename(item)), 'r') as ctrl_file:
                return self.__class__.FILES[item](ctrl_file.read())
        except KeyError:
            raise ControllerFileNotFoundError(cgroup=self.cgroup, controller=self, fil=item) from None
        except OSError as err:
            raise ControllerReadError(cgroup=self.cgroup, controller=self, fil=item) from err

    def make_filename(self, fil: str) -> str:
        return f'{self.__class__.PREFIX}.{fil}'

    def __str__(self):
        return f'{self.__class__.PREFIX} of cgroup {self.cgroup}'

    def __repr__(self):
        return f'{self.__class__.__name__}(cgroup={repr(self.cgroup)})'
