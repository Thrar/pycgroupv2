from __future__ import annotations

import typing

if typing.TYPE_CHECKING:
    from .formatters import Formatter

from .abstract_controller import AbstractController
from . import formatters as fmt


class CoreController(AbstractController):
    PREFIX: str = 'cgroup'

    FILES: dict[str, Formatter] = dict(
        controllers=fmt.string_list,
        procs=fmt.integer_list,
        subtree_control=fmt.string_list,
    )

    def __str__(self):
        return f'core of cgroup {self.cgroup}'
