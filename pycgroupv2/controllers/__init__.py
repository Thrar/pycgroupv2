from __future__ import annotations

import typing

if typing.TYPE_CHECKING:
    from typing import Type

from .abstract_controller import AbstractController as Controller
from .core import CoreController
from .irq import IrqController
from .memory import MemoryController

CONTROLLERS: dict[str, Type[Controller]] = dict(
    cgroup=CoreController,
    irq=IrqController,
    memory=MemoryController,
)
