from __future__ import annotations

import os
import typing

from .controllers import CONTROLLERS
from .error import ControllerDisabledError

DEFAULT_ROOT_PATH = '/sys/fs/cgroup/'


def from_path(path: str, root: str = DEFAULT_ROOT_PATH) -> CGroup:
    if path[:len(root)] != root:
        raise ValueError(f'path "{path}" is not under the CGroup root "{root}"')
    rel_path = path[len(root):]
    if rel_path == '':
        return ROOT_CGROUP

    def make_cgroup_chain(chain, cgroup) -> CGroup:
        if not chain:
            return CGroup(cgroup, ROOT_CGROUP)
        else:
            return CGroup(cgroup, make_cgroup_chain(*os.path.split(chain)))

    return make_cgroup_chain(*os.path.split(rel_path))


class CGroup:
    def __init__(self, name: str, parent: CGroup | None = None):
        self.name: str = name

        self.parent: CGroup | None = parent
        self.children: typing.Collection[CGroup] = []
        if self.parent is not None:
            self.parent.children.append(self)

        self.path = os.path.join(self.parent.path if self.parent is not None else '', self.name)

    def __getattr__(self, item):
        # The core controller is always "enabled".
        if item == 'cgroup':
            return CONTROLLERS[item](self)

        if item == 'irq':
            # Special case: irq is like an alias to the core controller, and does not appear in enabled controllers.
            return CONTROLLERS[item](self)

        if item not in self.cgroup.controllers:
            raise ControllerDisabledError(cgroup=self, controller_name=item)

        try:
            return CONTROLLERS[item](self)
        except KeyError:
            raise AttributeError(f'unknown cgroup controller {item}') from None

    def __str__(self):
        return self.name

    def __repr__(self):
        return f'{self.__class__.__name__}(name={self.name},parent={repr(self.parent)})'


ROOT_CGROUP = CGroup('<root>', None)
ROOT_CGROUP.path = DEFAULT_ROOT_PATH
