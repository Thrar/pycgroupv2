from __future__ import annotations

import typing

if typing.TYPE_CHECKING:
    from . import CGroup
    from .controllers import Controller


class Error(Exception):
    def __init__(self, cgroup: CGroup):
        self.cgroup = cgroup

        self.message = f'error in cgroup {self.cgroup}'

    def __str__(self):
        return self.message


class ControllerDisabledError(Error):
    def __init__(self, cgroup: CGroup, controller_name: str):
        super().__init__(cgroup)

        self.controller_name = controller_name

        self.message = f'controller {self.controller_name} is not enabled in cgroup {self.cgroup}'


class ControllerError(Error):
    def __init__(self, cgroup: CGroup, controller: Controller):
        super().__init__(cgroup)

        self.controller = controller

        self.message = f'error with controller {self.controller} in cgroup {self.cgroup}'


class ControllerFileNotFoundError(ControllerError):
    def __init__(self, cgroup: CGroup, controller: Controller, fil: str):
        super().__init__(cgroup, controller)

        self.fil = fil

        self.message = f'controller {self.controller} has no file named {self.controller.make_filename(self.fil)}'


class ControllerReadError(ControllerError):
    def __init__(self, cgroup: CGroup, controller: Controller, fil: str):
        super().__init__(cgroup, controller)

        self.fil = fil

        self.message = f'error reading {self.controller.make_filename(self.fil)} in cgroup {self.cgroup}'
