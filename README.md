# PyCGroupv2

Python interface to Linux Control Group v2.

Install with pip (or pipenv):

```shell
pip install git+https://gitlab.com/Thrar/pycgroupv2.git#egg=pycgroupv2
```

Usage:

```python
from pycgroupv2 import from_path

cg = from_path('/sys/fs/cgroup/user.slice')

print(f'current memory usage of users: {cg.memory.current / 1024 / 1024 / 1024:.2f}GiB')
```